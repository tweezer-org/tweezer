var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');
var cors = require('cors')

var indexRouter = require('./app/routes/index.routes');
var usersRouter = require('./app/routes/user.routes');
var authRouter = require('./app/routes/auth.routes');
var userRoleRouter = require('./app/routes/user_role.routes');
var hashtagRouter = require('./app/routes/hashtag.routes');

var app = express();


// CORS
var corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
};

app.use(cors(corsOption));


// Express
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// Passport
app.use(session({
  secret: 's3cr3t',
  resave: true,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());


// Routes
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/hashtags', hashtagRouter);
app.use('/roles', userRoleRouter);


// Mongoose
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/tweezer', { useNewUrlParser: true })
  .then(() =>  console.log('connection successful'))
  .catch((err) => console.error(err));

module.exports = app;
