var chai = require('chai');
var sinon = require('sinon');
var expect = chai.expect;
var authHelper = require('./../../app/routes/auth.helper');


describe('Auth helper', () => {
  describe('isLoggedIn', () => {
    it('should call isAuthenticated method', () => {
      var req = { isAuthenticated: sinon.spy() };
      var res = { status: sinon.spy() };
      var next = sinon.spy();

      authHelper.isLoggedIn(req, res, next);

      expect(req.isAuthenticated.calledOnce).to.be.true;
    });

    it('should go next on authenticated users', () => {
      var req = { isAuthenticated: sinon.stub().returns(true) };
      var res = { status: sinon.spy() };
      var next = sinon.spy();

      authHelper.isLoggedIn(req, res, next);

      expect(next.calledOnce).to.be.true;
      expect(res.status.notCalled).to.be.true;
    });

    it('should return status 401 on unauthenticated calls', () => {
      var req = { isAuthenticated: sinon.stub().returns(false) };
      var res = { status: sinon.spy() };
      var next = sinon.spy();

      authHelper.isLoggedIn(req, res, next);

      expect(next.notCalled).to.be.true;
      expect(res.status.calledWith(401)).to.be.true;
    });
  });

  describe('isAdmin', () => {
    it('should check if the user is admin', () => {
      var req = { user: sinon.spy() };
      var res = { status: sinon.spy() };
      var next = sinon.spy();

      authHelper.isAdmin(req, res, next);

    })
  });
})
