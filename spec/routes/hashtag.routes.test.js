var chai = require('chai')
var expect = chai.expect;
var should = chai.should();
var chaiHttp = require('chai-http');
var server = require('../../app');


chai.use(chaiHttp);


describe('hashtag routes', () => {
  it('should list ALL hashtags on /hashtags GET', function(done) {
    chai.request(server)
      .get('/hashtags')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
})

