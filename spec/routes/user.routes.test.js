var chai = require('chai');
var sinon = require('sinon');
var expect = chai.expect;
var should = chai.should();
var chaiHttp = require('chai-http');
var server = require('../../app');


chai.use(chaiHttp);


describe('when user is logged in, user routes', () => {
  let auth;

  beforeEach(() => {

  });

  afterEach(() => {
  });


  it('should list all users', () => {
    auth = require('../../app/routes/auth.helper');

    sinon.stub(auth, 'isLoggedIn')
      .callsFake((req, res, next) => {
      });

    chai.request(server)
      .get('/users')
      .end((err, res) => {
        res.should.have.status(401);
      });

    auth.isLoggedIn.restore();
  })
})
