var expect = require('chai').expect;
var UserRole = require('./../../app/models/user_role');


describe('UserRole model', function() {
  it('should be invalid if title is empty', function(done) {
    var role = new UserRole();

    role.validate(function(err) {
      expect(err.errors.title).to.exist;
      done();
    });
  });
});
