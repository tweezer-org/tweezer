var expect = require('chai').expect;
var User = require('./../../app/models/user');


describe('User model', function() {
  it('should be invalid if name is empty', function(done) {
    var u = new User();

    u.validate(function(err) {
      expect(err.errors.name).to.exist;
      done();
    });
  });

  it('should be invalid if id is empty', function(done) {
    var u = new User();

    u.validate(function(err) {
      expect(err.errors.id).to.exist;
      done();
    });
  });
});
