var expect = require('chai').expect;
var Hashtag = require('./../../app/models/hashtag');

describe('Hshtag', function() {
  it('should be invalid if name is empty', function(done) {
    var h = new Hashtag();

    h.validate(function(err) {
      expect(err.errors.name).to.exist;
      done();
    });
  });
});
