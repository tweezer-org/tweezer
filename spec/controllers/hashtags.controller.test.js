var expect = require('chai').expect;
var sinon = require('sinon');
var Hashtag = require('./../../app/models/hashtag')
var hashtagsCtrl = require('./../../app/controllers/hashtags.controller');


describe('hashtags controller', () => {

  describe('index action', () => {
    let hashtags, stubbedFind;

    beforeEach(() => {
      var h1 = new Hashtag({name: '#h1'});
      var h2 = new Hashtag({name: '#h2'});

      hashtags = [ h1, h2 ];

      stubbedFind = sinon.stub(Hashtag, 'find').returns({
        populate: sinon.stub().returns({
          exec: sinon.stub().returns(
            Promise.resolve(hashtags)
          )
        })
      })
    });

    afterEach(() => {
      Hashtag.find.restore();
    });


    it('returns all hashtags', () => {
      var req = { params: {} };
      var res = {
        json: sinon.stub(),
        setHeader: sinon.spy()
      };

      hashtagsCtrl.index(req, res, null);

      expect(stubbedFind.calledOnce).to.be.true;
      setTimeout(() => expect(res.json.calledOnce).to.be.true, 0);
    });
  })
});
