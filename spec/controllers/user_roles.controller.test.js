var expect = require('chai').expect;
var sinon = require('sinon');
var UserRole = require('./../../app/models/user_role');
var userRolesCtrl = require('./../../app/controllers/user_roles.controller');


describe('user_roles controller', () => {

  describe('index action', () => {
    let roles;

    beforeEach(() => {
      var r1 = new UserRole({ title: 'r1' });
      var r2 = new UserRole({ title: 'r2' });

      roles = [ r1, r2 ];

      sinon.stub(UserRole, 'find').resolves(roles);
    });

    afterEach(() => {
      UserRole.find.restore();
    });


    it('returns all roles', () => {
      var req = { params: {} };

      var res = { json: sinon.spy() };

      userRolesCtrl.index(req, res , null);

      setTimeout(() => expect(res.json.calledWith(roles)).to.be.true, 0);
    });
  });


  describe('touch method', () => {
    let stubbedFindOne;

    let r1 = new UserRole({ title: 'r1' });
    let r2 = new UserRole({ title: 'r2' });


    beforeEach(() => {
      stubbedFindOne = sinon.stub(UserRole, 'findOne');
    });


    afterEach(() => {
      UserRole.findOne.restore();
    });


    it('should find already existed roles', () => {
      stubbedFindOne.callsArgWith(1, null, r1);

      userRolesCtrl.touch('r1', (err, role) => {
        expect(role).to.be.equal(r1);
      });
    });


    it('should create the role if it is not existed', () => {
      stubbedFindOne.callsArgWith(1, 'err', null);

      var stubbedCreate = sinon.stub(UserRole, 'create');
      stubbedCreate.callsArgWith(1, null, r2);

      userRolesCtrl.touch('r2', (err, role) => {
        expect(role).to.be.equal(r2);
      });
    });
  })
})
