var expect = require('chai').expect;
var sinon = require('sinon');
var User = require('./../../app/models/user');
var usersCtrl = require('./../../app/controllers/users.controller');
var UserRole = require('./../../app/models/user_role');
var userRolesCtrl = require('./../../app/controllers/user_roles.controller');


describe('users controller', () => {

  let u1 = new User({ name: 'u1', id: 'id1', roles: ['user']});
  let u2 = new User({ name: 'u2', id: 'id2', roles: ['user']});

  describe('index action', () => {
    let users, stubbedUserFind;

    beforeEach(() => {

      users = [ u1, u2 ];

      stubbedUserFind = sinon.stub(User, 'find').returns({
        populate: sinon.stub().returns({
          exec: sinon.stub().returns(
            Promise.resolve(users)
          )
        })
      })
    });

    afterEach(() => {
      User.find.restore();
    });


    it('returns all users', () => {
      var req = { params: {} };
      var res = {
        json: sinon.spy(),
        setHeader: sinon.spy()
      };

      usersCtrl.index(req, res, null);

      expect(stubbedUserFind.calledOnce).to.be.true;
      setTimeout(() => expect(res.json.calledWith(users)).to.be.true, 0);
    });
  });

  describe('touch twitter user method', () => {

    let stubbedFindOne;

    beforeEach(() => {
      stubbedFindOne = sinon.stub(User, 'findOne');
    })

    afterEach(() => {
      User.findOne.restore();
    });


    it('should find already existed users', () => {
      var profile = { id: u1._id };

      stubbedFindOne.callsArgWith(1, null, u1);

      usersCtrl.touch_twitter_user(profile, (err, user) => {
          expect(user).to.be.equal(u1);
        });
    });


    it('should create a user', () => {
      var profile = { id: u2._id };
      stubbedFindOne.callsArgWith(1, 'err', null);

      var role = new UserRole({ name: 'r1' });

      var stubbedUserRolesCtrlTouch = sinon.stub(userRolesCtrl, 'touch');
      stubbedUserRolesCtrlTouch.resolves(role);

      var stubbedUserCreate = sinon.stub(User, 'create')
      stubbedUserCreate.resolves(u2);

      usersCtrl.touch_twitter_user(profile, (err, user) => {
          expect(user).to.be.equal(u2);
        });
    });
  });
})
