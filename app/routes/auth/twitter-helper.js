var fs = require('fs');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var request = require('request');


let cert = fs.readFileSync(process.env.RSAFile);


var createToken = (auth) => {
  return jwt.sign({ id: auth.id }, cert, { expiresIn: 60 * 120 });
};


exports.requestToken = (req, res, next) => {
  request.post(
    {
      url: 'https://api.twitter.com/oauth/request_token',
      oauth: {
        oauth_callback: "http://127.0.0.1:3000/auth/twitter/callback",
        consumer_key: process.env.twitterConsumerKey,
        consumer_secret: process.env.twitterConsumerSecret,
      }
    },
    function (err, r, body)
    {
      if (err)
        return res.send(500, { message: err.message });

      var jsonStr = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
      res.send(JSON.parse(jsonStr));

      return next();
    }
  );
}


exports.authVerifier = (req, res, next) => {
  request.post(
    {
      url: `https://api.twitter.com/oauth/access_token?oauth_verifier`,
      oauth:
      {
        consumer_key: process.env.twitterConsumerKey,
        consumer_secret: process.env.twitterConsumerSecret,
        token: req.query.oauth_token
      },
      form: { oauth_verifier: req.query.oauth_verifier }
    },
    (err, r, body) =>
    {
      if (err)
        return res.send(500, { message: err.message });

      const bodyString = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
      const parsedBody = JSON.parse(bodyString);

      req.body['oauth_token'] = parsedBody.oauth_token;
      req.body['oauth_token_secret'] = parsedBody.oauth_token_secret;
      req.body['user_id'] = parsedBody.user_id;

      return next();
    }
  );
}


exports.authenticate = expressJwt({
  secret: cert,
  requestProperty: 'auth',
  getToken: function(req) {
    if (req.headers['x-auth-token'])
      return req.headers['x-auth-token'];

    return null;
  }
});


exports.doesUserExist = (req, res, next) => {
  if (!req.user)
    return res.send(401, 'User Not Authenticated');

  return next();
}


exports.prepareTokenForAPI = (req, res, next) => {
  req.auth = { id: req.user.id };
  return next();
}


exports.generateToken = (req, res, next) => {
  req.token = createToken(req.auth);
  return next();
};


exports.sendToken = (req, res) => {
  res.setHeader('x-auth-token', req.token);
  return res.status(200).send(JSON.stringify(req.user));
};
