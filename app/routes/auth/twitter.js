var passport = require('passport')
var TwitterTokenStrategy = require('passport-twitter-token');
var User = require('./../../models/user');
var UserRole = require('./../../models/user_role');
var usersCtrl = require('./../../controllers/users.controller')
var request = require('request');


passport.use(new TwitterTokenStrategy(
  {
    consumerKey: process.env.twitterConsumerKey,
    consumerSecret: process.env.twitterConsumerSecret,
    callbackURL: "http://127.0.0.1:3000/auth/twitter/callback"
  },
  (accessToken, refreshToken, profile, done) => {
    usersCtrl.touch_twitter_user(profile, (err, user) => {
      if (!user)
        return done(err, user);
      else
        return done(null, user);
    })
  }
));


passport.serializeUser( (user, fn) => {
  fn(null, user);
});


passport.deserializeUser( (id, fn) => {
  User.findOne({_id: id.doc._id}, (err, user) => {
    fn(err, user);
  });
});


module.exports = passport;

