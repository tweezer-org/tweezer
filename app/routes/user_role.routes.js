var express = require('express');
var router = express.Router();
var authUtil = require('./auth.helper');
var userRolesCtrl = require('./../controllers/user_roles.controller');


router.get('/', authUtil.isLoggedIn, userRolesCtrl.index);


module.exports = router;
