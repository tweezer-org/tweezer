var passport = require('passport')
var express = require('express');
var router = express.Router();
var twitter = require('./auth/twitter');
var twitterHelper = require('./auth/twitter-helper')
var webToken = require('./../controllers/web_token.controller');
var authHelper = require('./auth.helper')


// twitter router
router.post('/twitter/reverse',
  twitterHelper.requestToken);

router.post('/twitter',
  twitterHelper.authVerifier,
  passport.authenticate('twitter-token', {session: false}),
  twitterHelper.doesUserExist,
  twitterHelper.prepareTokenForAPI,
  twitterHelper.generateToken,
  twitterHelper.sendToken
);

router.get('/twitter/callback',
  (req, res) => {
    // Successful authentication, redirect home.
    res.redirect('http://127.0.0.1:3000');
  });


module.exports = router;


