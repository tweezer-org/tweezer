var express = require('express');
var router = express.Router();
var hashtagsCtrl = require('./../controllers/hashtags.controller');


router.get('/', hashtagsCtrl.index);
router.get('/:id', hashtagsCtrl.show);
router.post('/', hashtagsCtrl.create); // TODO: check authentication


module.exports = router;
