exports.isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) 
    return next();

  res.status(401);
}

exports.isAdmin = (req, res, next) => {
  console.log(req);
  return next();
}
