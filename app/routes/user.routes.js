var express = require('express');
var router = express.Router();
var authUtil = require('./auth.helper')
var usersCtrl = require('./../controllers/users.controller')


// Get user listing as json object
router.get('/', authUtil.isAdmin, usersCtrl.index); // TODO: check admin


module.exports = router;
