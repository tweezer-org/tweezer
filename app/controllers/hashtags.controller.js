var Hashtag = require('./../models/hashtag');


exports.index = (req, res, next) => {
  Hashtag.find().populate('User').exec()
    .then(hashtags => {
      res.json(hashtags);
    })
    .catch(err => {
      res.json(err);
    })
}


exports.show = (req, res, next) => {
  Hashtag.findById(req.params.id)
    .then(hashtag => {
      res.json(hashtag);
    })
    .catch(err => {
      res.json(err);
    })
}


exports.create = (req, res, next) => {
  Hashtag.create({name: req.params.name})
    .then(hashtag => {
      res.json(hashtag);
    })
    .catch(err => {
      res.json(err);
    })
}
