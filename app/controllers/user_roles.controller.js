var UserRole = require('./../models/user_role');


exports.roles = {
  user: 'user',
  admin: 'admin',
}


exports.index = (req, res, next) => {
  UserRole.find()
    .then((role) => {
      res.json(role);
    })
    .catch(err => {
      res.json(err);
    })
}


exports.touch = (title, cb) => {
  return UserRole.findOne({'title': title}, (err, role) => {
    if (role == null) {
      UserRole.create({title: title}, (err, role) => {
        if (err) return cb(err, null);

        return cb(null, role);
      })
    }
    else
    {
      return cb(null, role);
    }
  });
}
