var User = require('./../models/user');
var userRolesCtrl = require('./user_roles.controller');
var UserRole = require('./../models/user_role');


exports.index = (req, res, next) => {
  User.find().populate('UserRole').exec()
    .then(users => {
      res.json(users);
    })
    .catch(err => {
      res.json(err);
    })
}


exports.touch_twitter_user = (profile, cb) => {
  return User.findOne({'id': profile.id}, (err, user) => {
    if (user == null) {
      userRolesCtrl.touch(userRolesCtrl.roles.user, (err, role) => {
        if (role == null) return cb(err, null);

        User.create(
          {
            id: profile.id,
            name: profile.displayName,
            roles: [role._id],
          },
          (err, user) => {
            if (err) return cb(err, null);

            return cb(null, user);
          }
        )
      })
    }
    else
    {
      return cb(null, user);
    }
  });
}
