var mongoose = require('mongoose');


var HashtagSchema = new mongoose.Schema({
  name: {type: String, required: true},
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
});


module.exports = mongoose.model('Hashtag', HashtagSchema);
