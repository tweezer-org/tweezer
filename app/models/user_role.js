var mongoose = require('mongoose');


var UserRoleSchema = new mongoose.Schema({
  title: {type: String, required: true},
});


module.exports = mongoose.model('UserRole', UserRoleSchema, 'user_roles');
