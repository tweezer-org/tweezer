var mongoose = require('mongoose');


var UserSchema = new mongoose.Schema({
  name: {type: String, required: true},
  id: {type: String, required: true},
  updated_at: { type: Date, default: Date.now },
  roles: [{ type: mongoose.Schema.Types.ObjectId, ref: 'UserRole' }],
});


module.exports = mongoose.model('User', UserSchema, 'users');
