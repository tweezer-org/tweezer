# Tweezer
Twitter analyzer

# Routes

```mermaid
graph TD
A((/)) --> B(users)
B --> C[index]

style C fill:#ffbb33,stroke-width:0px
```

```mermaid
graph TD
A((/)) --> B(auth)
B --> C(twitter - login)
C --> D(reverse - request token)
```

```mermaid
graph TD
A((/)) --> B(hashtags)
B --> C[index]
B --> D[show]
B --> E[create]

style C fill:#00c851,stroke-width:0px
style D fill:#00c851,stroke-width:0px
style E fill:#00c851,stroke-width:0px
```

```mermaid
graph TD
A((/)) --> B(roles)
B --> C[index]

style C fill:#ffbb33,stroke-width:0px
```
